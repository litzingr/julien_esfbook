# ESFBOOK #

ESFBOOK is a website originally written with emberjs and php as a backend. This is it being upgraded to use a newer version of emberjs and rails as a backend.

### Dependencies ###

### Mac OS X ###
Make sure you have xcode developer tools, and homebrew installed before running this.

Make sure you install these on the machine
``````
npm
* node & nam
* postgres
* ember-cli
* watchman
gem
* bundler
* pg

``````
Make sure you have a SQL server like [PostgreSQL](http://www.postgresql.org) installed on your machine
install dependencies in the `/rails/` folder using `bundle install`
Serve the front end with `ember server` in the `/esfbook/` directory
Serve the rails server with `rails server`

look at tests at `https://localhost:4200/tests`

[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)