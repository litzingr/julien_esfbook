import Ember from 'ember';

export default Ember.Controller.extend({
  i18n: Ember.inject.service(),
  actions:{
    makeFrActive: function(){
      const i18n = this.get('i18n');
      i18n.set('locale', 'fr');
    },
    makeEnActive: function(){
      const i18n = this.get('i18n');
      i18n.set('locale', 'en');
    }
  },
  locales: Ember.computed('i18n.locale', 'i18n.locales', function() {
    const i18n = this.get('i18n');
    let isFr;
    let isEn;
    if (this.get('i18n.locale') === 'fr'){
      isFr = 'active';
      isEn = '';
    } else {
      isEn = 'active';
      isFr = '';
    }
    return { id: this.get('i18n.locale'), isFr: isFr, isEn: isEn,accountAccess: i18n.t('user.access'), terms: i18n.t('terms'), credits: i18n.t('credits')};
  }),
});
