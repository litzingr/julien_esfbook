import Ember from 'ember';

export default Ember.Controller.extend({
  i18n: Ember.inject.service(),
  locales: Ember.computed('i18n.locale', 'i18n.locales', function() {
    const i18n = this.get('i18n');
    return { id: this.get('i18n.locale'), codeInstructions: i18n.t('lookup.instructions'), submit: i18n.t('lookup.submit')};
  }),
});
