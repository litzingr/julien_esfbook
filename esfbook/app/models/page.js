import Model from 'ember-data/model';

export default Model.extend({
  messages: DS.hasMany('message')
});
