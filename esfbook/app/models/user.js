// app/user/model.js

import DS from 'ember-data';

export default DS.Model.extend({
  username: DS.attr('string')
});
