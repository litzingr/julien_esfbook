import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('home', { path: '/'});
  this.route('user', { path: '/user/:id'}, function() {
    this.route('login');
  });
  this.route('page', { path: '/page/:id'});
  this.route('404');
});

export default Router;
