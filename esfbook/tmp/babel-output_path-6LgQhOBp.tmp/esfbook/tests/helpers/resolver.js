define('esfbook/tests/helpers/resolver', ['exports', 'esfbook/resolver', 'esfbook/config/environment'], function (exports, _esfbookResolver, _esfbookConfigEnvironment) {

  var resolver = _esfbookResolver['default'].create();

  resolver.namespace = {
    modulePrefix: _esfbookConfigEnvironment['default'].modulePrefix,
    podModulePrefix: _esfbookConfigEnvironment['default'].podModulePrefix
  };

  exports['default'] = resolver;
});