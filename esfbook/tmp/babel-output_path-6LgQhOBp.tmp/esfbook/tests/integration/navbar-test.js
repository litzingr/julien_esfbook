define('esfbook/tests/integration/navbar-test', ['exports', 'ember', 'qunit', 'esfbook/tests/helpers/start-app'], function (exports, _ember, _qunit, _esfbookTestsHelpersStartApp) {

  var App;

  (0, _qunit.module)('Integration - Landing Page', {
    beforeEach: function beforeEach() {
      App = (0, _esfbookTestsHelpersStartApp['default'])();
    },
    afterEach: function afterEach() {
      _ember['default'].run(App, 'destroy');
    }
  });

  (0, _qunit.test)('Should welcome me to Boston Ember', function (assert) {
    visit('/').then(function () {
      assert.equal(true, true);
    });
  });
});