define('esfbook/tests/components/instructor-frame.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/instructor-frame.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/instructor-frame.js should pass jshint.');
  });
});