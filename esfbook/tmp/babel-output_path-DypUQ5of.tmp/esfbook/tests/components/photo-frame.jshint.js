define('esfbook/tests/components/photo-frame.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/photo-frame.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/photo-frame.js should pass jshint.');
  });
});