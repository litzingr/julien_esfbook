define('esfbook/tests/components/school-frame.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/school-frame.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/school-frame.js should pass jshint.');
  });
});