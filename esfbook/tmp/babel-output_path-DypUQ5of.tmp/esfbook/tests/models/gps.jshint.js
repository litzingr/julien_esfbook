define('esfbook/tests/models/gps.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/gps.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/gps.js should pass jshint.');
  });
});