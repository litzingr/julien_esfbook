define('esfbook/tests/models/instructor.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/instructor.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/instructor.js should pass jshint.');
  });
});