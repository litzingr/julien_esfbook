define('esfbook/tests/models/page.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/page.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(false, 'models/page.js should pass jshint.\nmodels/page.js: line 4, col 13, \'DS\' is not defined.\n\n1 error');
  });
});