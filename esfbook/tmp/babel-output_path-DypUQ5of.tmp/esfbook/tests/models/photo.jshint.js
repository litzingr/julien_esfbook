define('esfbook/tests/models/photo.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/photo.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/photo.js should pass jshint.');
  });
});