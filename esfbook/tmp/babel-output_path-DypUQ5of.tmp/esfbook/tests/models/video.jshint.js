define('esfbook/tests/models/video.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | models/video.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/video.js should pass jshint.');
  });
});