define('esfbook/tests/integration/components/photo-frame-test.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | integration/components/photo-frame-test.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/photo-frame-test.js should pass jshint.');
  });
});