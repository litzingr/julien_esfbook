define('esfbook/tests/integration/components/video-frame-test.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | integration/components/video-frame-test.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/video-frame-test.js should pass jshint.');
  });
});