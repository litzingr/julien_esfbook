define('esfbook/tests/helpers/start-app', ['exports', 'ember', 'esfbook/app', 'esfbook/config/environment'], function (exports, _ember, _esfbookApp, _esfbookConfigEnvironment) {
  exports['default'] = startApp;

  function startApp(attrs) {
    var application = undefined;

    var attributes = _ember['default'].merge({}, _esfbookConfigEnvironment['default'].APP);
    attributes = _ember['default'].merge(attributes, attrs); // use defaults, but you can override;

    _ember['default'].run(function () {
      application = _esfbookApp['default'].create(attributes);
      application.setupForTesting();
      application.injectTestHelpers();
    });

    return application;
  }
});