define('esfbook/tests/test-helper', ['exports', 'esfbook/tests/helpers/resolver', 'ember-qunit'], function (exports, _esfbookTestsHelpersResolver, _emberQunit) {

  (0, _emberQunit.setResolver)(_esfbookTestsHelpersResolver['default']);
});