define('esfbook/components/app-version', ['exports', 'ember-cli-app-version/components/app-version', 'esfbook/config/environment'], function (exports, _emberCliAppVersionComponentsAppVersion, _esfbookConfigEnvironment) {

  var name = _esfbookConfigEnvironment['default'].APP.name;
  var version = _esfbookConfigEnvironment['default'].APP.version;

  exports['default'] = _emberCliAppVersionComponentsAppVersion['default'].extend({
    version: version,
    name: name
  });
});