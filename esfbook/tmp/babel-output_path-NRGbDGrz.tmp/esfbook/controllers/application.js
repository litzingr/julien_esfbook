define('esfbook/controllers/application', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Controller.extend({
    i18n: _ember['default'].inject.service(),
    actions: {
      makeFrActive: function makeFrActive() {
        var i18n = this.get('i18n');
        i18n.set('locale', 'fr');
      },
      makeEnActive: function makeEnActive() {
        var i18n = this.get('i18n');
        i18n.set('locale', 'en');
      }
    },
    locales: _ember['default'].computed('i18n.locale', 'i18n.locales', function () {
      var i18n = this.get('i18n');
      var isFr = undefined;
      var isEn = undefined;
      if (this.get('i18n.locale') === 'fr') {
        isFr = 'active';
        isEn = '';
      } else {
        isEn = 'active';
        isFr = '';
      }
      return { id: this.get('i18n.locale'), isFr: isFr, isEn: isEn, accountAccess: i18n.t('user.access'), terms: i18n.t('terms'), credits: i18n.t('credits') };
    })
  });
});