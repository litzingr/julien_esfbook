define('esfbook/initializers/app-version', ['exports', 'ember-cli-app-version/initializer-factory', 'esfbook/config/environment'], function (exports, _emberCliAppVersionInitializerFactory, _esfbookConfigEnvironment) {
  exports['default'] = {
    name: 'App Version',
    initialize: (0, _emberCliAppVersionInitializerFactory['default'])(_esfbookConfigEnvironment['default'].APP.name, _esfbookConfigEnvironment['default'].APP.version)
  };
});