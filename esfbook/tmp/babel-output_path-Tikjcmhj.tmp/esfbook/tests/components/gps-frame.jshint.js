define('esfbook/tests/components/gps-frame.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/gps-frame.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/gps-frame.js should pass jshint.');
  });
});