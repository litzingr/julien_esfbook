define('esfbook/tests/components/picture-frame.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | components/picture-frame.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/picture-frame.js should pass jshint.');
  });
});