define('esfbook/tests/routes/page.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | routes/page.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/page.js should pass jshint.');
  });
});