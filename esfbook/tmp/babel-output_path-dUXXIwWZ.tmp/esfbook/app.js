define('esfbook/app', ['exports', 'ember', 'esfbook/resolver', 'ember-load-initializers', 'esfbook/config/environment'], function (exports, _ember, _esfbookResolver, _emberLoadInitializers, _esfbookConfigEnvironment) {

  var App = undefined;

  _ember['default'].MODEL_FACTORY_INJECTIONS = true;

  App = _ember['default'].Application.extend({
    modulePrefix: _esfbookConfigEnvironment['default'].modulePrefix,
    podModulePrefix: _esfbookConfigEnvironment['default'].podModulePrefix,
    Resolver: _esfbookResolver['default']
  });

  (0, _emberLoadInitializers['default'])(App, _esfbookConfigEnvironment['default'].modulePrefix);

  exports['default'] = App;
});