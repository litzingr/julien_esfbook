define('esfbook/controllers/home', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Controller.extend({
    i18n: _ember['default'].inject.service(),
    locales: _ember['default'].computed('i18n.locale', 'i18n.locales', function () {
      var i18n = this.get('i18n');
      return { id: this.get('i18n.locale'), codeInstructions: i18n.t('lookup.instructions'), submit: i18n.t('lookup.submit') };
    })
  });
});