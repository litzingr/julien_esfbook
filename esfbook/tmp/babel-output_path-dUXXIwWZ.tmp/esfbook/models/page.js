define('esfbook/models/page', ['exports', 'ember-data/model'], function (exports, _emberDataModel) {
  exports['default'] = _emberDataModel['default'].extend({
    messages: DS.hasMany('message')
  });
});