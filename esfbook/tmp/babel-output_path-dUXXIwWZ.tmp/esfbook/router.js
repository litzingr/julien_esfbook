define('esfbook/router', ['exports', 'ember', 'esfbook/config/environment'], function (exports, _ember, _esfbookConfigEnvironment) {

  var Router = _ember['default'].Router.extend({
    location: _esfbookConfigEnvironment['default'].locationType
  });

  Router.map(function () {
    this.route('home', { path: '/' });
    this.route('user', { path: '/user/:id' }, function () {
      this.route('login');
    });
    this.route('page', { path: '/page/:id' });
    this.route('404');
  });

  exports['default'] = Router;
});