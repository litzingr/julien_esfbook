define('esfbook/tests/locales/fr/translations.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | locales/fr/translations.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'locales/fr/translations.js should pass jshint.');
  });
});