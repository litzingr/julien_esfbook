define("esfbook/initializers/ember-i18n", ["exports", "esfbook/instance-initializers/ember-i18n"], function (exports, _esfbookInstanceInitializersEmberI18n) {
  exports["default"] = {
    name: _esfbookInstanceInitializersEmberI18n["default"].name,

    initialize: function initialize() {
      var application = arguments[1] || arguments[0]; // depending on Ember version
      if (application.instanceInitializer) {
        return;
      }

      _esfbookInstanceInitializersEmberI18n["default"].initialize(application);
    }
  };
});