define('esfbook/tests/integration/components/picture-frame-test.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | integration/components/picture-frame-test.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/picture-frame-test.js should pass jshint.');
  });
});