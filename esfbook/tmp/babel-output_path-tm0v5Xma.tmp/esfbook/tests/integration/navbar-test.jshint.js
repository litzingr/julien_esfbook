define('esfbook/tests/integration/navbar-test.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | integration/navbar-test.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/navbar-test.js should pass jshint.');
  });
});