define('esfbook/tests/integration/components/message-frame-test.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint | integration/components/message-frame-test.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/message-frame-test.js should pass jshint.');
  });
});