"use strict";

/* jshint ignore:start */



/* jshint ignore:end */

define('esfbook/app', ['exports', 'ember', 'esfbook/resolver', 'ember-load-initializers', 'esfbook/config/environment'], function (exports, _ember, _esfbookResolver, _emberLoadInitializers, _esfbookConfigEnvironment) {

  var App = undefined;

  _ember['default'].MODEL_FACTORY_INJECTIONS = true;

  App = _ember['default'].Application.extend({
    modulePrefix: _esfbookConfigEnvironment['default'].modulePrefix,
    podModulePrefix: _esfbookConfigEnvironment['default'].podModulePrefix,
    Resolver: _esfbookResolver['default']
  });

  (0, _emberLoadInitializers['default'])(App, _esfbookConfigEnvironment['default'].modulePrefix);

  exports['default'] = App;
});
define('esfbook/components/app-version', ['exports', 'ember-cli-app-version/components/app-version', 'esfbook/config/environment'], function (exports, _emberCliAppVersionComponentsAppVersion, _esfbookConfigEnvironment) {

  var name = _esfbookConfigEnvironment['default'].APP.name;
  var version = _esfbookConfigEnvironment['default'].APP.version;

  exports['default'] = _emberCliAppVersionComponentsAppVersion['default'].extend({
    version: version,
    name: name
  });
});
define('esfbook/components/comment-frame', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Component.extend({});
});
define('esfbook/components/gps-frame', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Component.extend({});
});
define('esfbook/components/message-frame', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Component.extend({});
});
define('esfbook/components/photo-frame', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Component.extend({});
});
define('esfbook/components/picture-frame', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Component.extend({});
});
define('esfbook/components/video-frame', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Component.extend({});
});
define('esfbook/controllers/application', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Controller.extend({
    i18n: _ember['default'].inject.service(),
    actions: {
      makeFrActive: function makeFrActive() {
        var i18n = this.get('i18n');
        i18n.set('locale', 'fr');
      },
      makeEnActive: function makeEnActive() {
        var i18n = this.get('i18n');
        i18n.set('locale', 'en');
      }
    },
    locales: _ember['default'].computed('i18n.locale', 'i18n.locales', function () {
      var i18n = this.get('i18n');
      var isFr = undefined;
      var isEn = undefined;
      if (this.get('i18n.locale') === 'fr') {
        isFr = 'active';
        isEn = '';
      } else {
        isEn = 'active';
        isFr = '';
      }
      return { id: this.get('i18n.locale'), isFr: isFr, isEn: isEn, accountAccess: i18n.t('user.access'), terms: i18n.t('terms'), credits: i18n.t('credits') };
    })
  });
});
define('esfbook/controllers/home', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Controller.extend({
    i18n: _ember['default'].inject.service(),
    locales: _ember['default'].computed('i18n.locale', 'i18n.locales', function () {
      var i18n = this.get('i18n');
      return { id: this.get('i18n.locale'), codeInstructions: i18n.t('lookup.instructions'), submit: i18n.t('lookup.submit') };
    })
  });
});
define('esfbook/helpers/pluralize', ['exports', 'ember-inflector/lib/helpers/pluralize'], function (exports, _emberInflectorLibHelpersPluralize) {
  exports['default'] = _emberInflectorLibHelpersPluralize['default'];
});
define('esfbook/helpers/singularize', ['exports', 'ember-inflector/lib/helpers/singularize'], function (exports, _emberInflectorLibHelpersSingularize) {
  exports['default'] = _emberInflectorLibHelpersSingularize['default'];
});
define('esfbook/helpers/t', ['exports', 'ember-i18n/helper'], function (exports, _emberI18nHelper) {
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberI18nHelper['default'];
    }
  });
});
define('esfbook/initializers/app-version', ['exports', 'ember-cli-app-version/initializer-factory', 'esfbook/config/environment'], function (exports, _emberCliAppVersionInitializerFactory, _esfbookConfigEnvironment) {
  exports['default'] = {
    name: 'App Version',
    initialize: (0, _emberCliAppVersionInitializerFactory['default'])(_esfbookConfigEnvironment['default'].APP.name, _esfbookConfigEnvironment['default'].APP.version)
  };
});
define('esfbook/initializers/container-debug-adapter', ['exports', 'ember-resolver/container-debug-adapter'], function (exports, _emberResolverContainerDebugAdapter) {
  exports['default'] = {
    name: 'container-debug-adapter',

    initialize: function initialize() {
      var app = arguments[1] || arguments[0];

      app.register('container-debug-adapter:main', _emberResolverContainerDebugAdapter['default']);
      app.inject('container-debug-adapter:main', 'namespace', 'application:main');
    }
  };
});
define('esfbook/initializers/data-adapter', ['exports', 'ember'], function (exports, _ember) {

  /*
    This initializer is here to keep backwards compatibility with code depending
    on the `data-adapter` initializer (before Ember Data was an addon).
  
    Should be removed for Ember Data 3.x
  */

  exports['default'] = {
    name: 'data-adapter',
    before: 'store',
    initialize: _ember['default'].K
  };
});
define('esfbook/initializers/ember-cli-mirage', ['exports', 'ember-cli-mirage/utils/read-modules', 'esfbook/config/environment', 'esfbook/mirage/config', 'ember-cli-mirage/server'], function (exports, _emberCliMirageUtilsReadModules, _esfbookConfigEnvironment, _esfbookMirageConfig, _emberCliMirageServer) {
  exports['default'] = {
    name: 'ember-cli-mirage',
    initialize: function initialize(application) {
      if (arguments.length > 1) {
        // Ember < 2.1
        var container = arguments[0],
            application = arguments[1];
      }
      var environment = _esfbookConfigEnvironment['default'].environment;

      if (_shouldUseMirage(environment, _esfbookConfigEnvironment['default']['ember-cli-mirage'])) {
        var modules = (0, _emberCliMirageUtilsReadModules['default'])(_esfbookConfigEnvironment['default'].modulePrefix);
        var options = _.assign(modules, { environment: environment, baseConfig: _esfbookMirageConfig['default'], testConfig: _esfbookMirageConfig.testConfig });

        new _emberCliMirageServer['default'](options);
      }
    }
  };

  function _shouldUseMirage(env, addonConfig) {
    var userDeclaredEnabled = typeof addonConfig.enabled !== 'undefined';
    var defaultEnabled = _defaultEnabled(env, addonConfig);

    return userDeclaredEnabled ? addonConfig.enabled : defaultEnabled;
  }

  /*
    Returns a boolean specifying the default behavior for whether
    to initialize Mirage.
  */
  function _defaultEnabled(env, addonConfig) {
    var usingInDev = env === 'development' && !addonConfig.usingProxy;
    var usingInTest = env === 'test';

    return usingInDev || usingInTest;
  }
});
define('esfbook/initializers/ember-data', ['exports', 'ember-data/setup-container', 'ember-data/-private/core'], function (exports, _emberDataSetupContainer, _emberDataPrivateCore) {

  /*
  
    This code initializes Ember-Data onto an Ember application.
  
    If an Ember.js developer defines a subclass of DS.Store on their application,
    as `App.StoreService` (or via a module system that resolves to `service:store`)
    this code will automatically instantiate it and make it available on the
    router.
  
    Additionally, after an application's controllers have been injected, they will
    each have the store made available to them.
  
    For example, imagine an Ember.js application with the following classes:
  
    App.StoreService = DS.Store.extend({
      adapter: 'custom'
    });
  
    App.PostsController = Ember.ArrayController.extend({
      // ...
    });
  
    When the application is initialized, `App.ApplicationStore` will automatically be
    instantiated, and the instance of `App.PostsController` will have its `store`
    property set to that instance.
  
    Note that this code will only be run if the `ember-application` package is
    loaded. If Ember Data is being used in an environment other than a
    typical application (e.g., node.js where only `ember-runtime` is available),
    this code will be ignored.
  */

  exports['default'] = {
    name: 'ember-data',
    initialize: _emberDataSetupContainer['default']
  };
});
define("esfbook/initializers/ember-i18n", ["exports", "esfbook/instance-initializers/ember-i18n"], function (exports, _esfbookInstanceInitializersEmberI18n) {
  exports["default"] = {
    name: _esfbookInstanceInitializersEmberI18n["default"].name,

    initialize: function initialize() {
      var application = arguments[1] || arguments[0]; // depending on Ember version
      if (application.instanceInitializer) {
        return;
      }

      _esfbookInstanceInitializersEmberI18n["default"].initialize(application);
    }
  };
});
define('esfbook/initializers/export-application-global', ['exports', 'ember', 'esfbook/config/environment'], function (exports, _ember, _esfbookConfigEnvironment) {
  exports.initialize = initialize;

  function initialize() {
    var application = arguments[1] || arguments[0];
    if (_esfbookConfigEnvironment['default'].exportApplicationGlobal !== false) {
      var value = _esfbookConfigEnvironment['default'].exportApplicationGlobal;
      var globalName;

      if (typeof value === 'string') {
        globalName = value;
      } else {
        globalName = _ember['default'].String.classify(_esfbookConfigEnvironment['default'].modulePrefix);
      }

      if (!window[globalName]) {
        window[globalName] = application;

        application.reopen({
          willDestroy: function willDestroy() {
            this._super.apply(this, arguments);
            delete window[globalName];
          }
        });
      }
    }
  }

  exports['default'] = {
    name: 'export-application-global',

    initialize: initialize
  };
});
define('esfbook/initializers/injectStore', ['exports', 'ember'], function (exports, _ember) {

  /*
    This initializer is here to keep backwards compatibility with code depending
    on the `injectStore` initializer (before Ember Data was an addon).
  
    Should be removed for Ember Data 3.x
  */

  exports['default'] = {
    name: 'injectStore',
    before: 'store',
    initialize: _ember['default'].K
  };
});
define('esfbook/initializers/store', ['exports', 'ember'], function (exports, _ember) {

  /*
    This initializer is here to keep backwards compatibility with code depending
    on the `store` initializer (before Ember Data was an addon).
  
    Should be removed for Ember Data 3.x
  */

  exports['default'] = {
    name: 'store',
    after: 'ember-data',
    initialize: _ember['default'].K
  };
});
define('esfbook/initializers/transforms', ['exports', 'ember'], function (exports, _ember) {

  /*
    This initializer is here to keep backwards compatibility with code depending
    on the `transforms` initializer (before Ember Data was an addon).
  
    Should be removed for Ember Data 3.x
  */

  exports['default'] = {
    name: 'transforms',
    before: 'store',
    initialize: _ember['default'].K
  };
});
define("esfbook/instance-initializers/ember-data", ["exports", "ember-data/-private/instance-initializers/initialize-store-service"], function (exports, _emberDataPrivateInstanceInitializersInitializeStoreService) {
  exports["default"] = {
    name: "ember-data",
    initialize: _emberDataPrivateInstanceInitializersInitializeStoreService["default"]
  };
});
define("esfbook/instance-initializers/ember-i18n", ["exports", "ember", "ember-i18n/stream", "ember-i18n/legacy-helper", "esfbook/config/environment"], function (exports, _ember, _emberI18nStream, _emberI18nLegacyHelper, _esfbookConfigEnvironment) {
  exports["default"] = {
    name: 'ember-i18n',

    initialize: function initialize(appOrAppInstance) {
      if (_emberI18nLegacyHelper["default"] != null) {
        (function () {
          // Used for Ember < 1.13
          var i18n = appOrAppInstance.container.lookup('service:i18n');

          i18n.localeStream = new _emberI18nStream["default"](function () {
            return i18n.get('locale');
          });

          _ember["default"].addObserver(i18n, 'locale', i18n, function () {
            this.localeStream.value(); // force the stream to be dirty
            this.localeStream.notify();
          });

          _ember["default"].HTMLBars._registerHelper('t', _emberI18nLegacyHelper["default"]);
        })();
      }
    }
  };
});
define("esfbook/locales/en/config", ["exports"], function (exports) {
  // Ember-I18n includes configuration for common locales. Most users
  // can safely delete this file. Use it if you need to override behavior
  // for a locale or define behavior for a locale that Ember-I18n
  // doesn't know about.
  exports["default"] = {
    // rtl: [true|FALSE],
    //
    // pluralForm: function(count) {
    //   if (count === 0) { return 'zero'; }
    //   if (count === 1) { return 'one'; }
    //   if (count === 2) { return 'two'; }
    //   if (count < 5) { return 'few'; }
    //   if (count >= 5) { return 'many'; }
    //   return 'other';
    // }
  };
});
define("esfbook/locales/en/translations", ["exports"], function (exports) {
    exports["default"] = {
        contact: "Support",
        credits: "Credits",
        "datepicker.days.monday": "Monday",
        "datepicker.days.tuesday": "Tuesday",
        "datepicker.days.wednesday": "Wednesday",
        "datepicker.days.thursday": "Thursday",
        "datepicker.days.friday": "Friday",
        "datepicker.days.saturday": "Saturday",
        "datepicker.days.sunday": "Sunday",
        "datepicker.days.short.monday": "Mon",
        "datepicker.days.short.tuesday": "Tue",
        "datepicker.days.short.wednesday": "Wed",
        "datepicker.days.short.thursday": "Thu",
        "datepicker.days.short.friday": "Fri",
        "datepicker.days.short.saturday": "Sat",
        "datepicker.days.short.sunday": "Sun",
        "datepicker.months.january": "January",
        "datepicker.months.february": "February",
        "datepicker.months.march": "March",
        "datepicker.months.april": "April",
        "datepicker.months.may": "May",
        "datepicker.months.june": "June",
        "datepicker.months.july": "July",
        "datepicker.months.august": "August",
        "datepicker.months.september": "September",
        "datepicker.months.october": "October",
        "datepicker.months.november": "November",
        "datepicker.months.december": "December",
        "datepicker.next": ">",
        "datepicker.previous": "<",
        "dropzone.multiple": "Click or drop your files here to upload",
        "dropzone.single": "Click or drop your file here to upload",
        "editor.bold": "Bold",
        "editor.italic": "Italic",
        "editor.link": "Link",
        "editor.link.cancel": "Cancel",
        "editor.link.ok": "OK",
        "editor.ordered": "Numbered list",
        "editor.underline": "Underline",
        "editor.unordered": "Unordered list",
        "error.content.not_found": "The requested content could not be found.",
        "error.page.not_found.message": "The page could not be found",
        "error.page.not_found.return": "Return to the home page",
        "error.server": "An error occured. Please try again in a few minutes.",
        "error.token.bad_credentials": "Incorrect username or password.",
        "error.token.general": "Login failed.",
        "error.token.missing_username": "Please enter a valid username.",
        "error.vimeo.general": "Your video could not be uploaded because of an issue with our provider. Please try again later.",
        help: "Help",
        "instructor.bio": "Biography",
        "instructor.email": "Contact me",
        "instructor.facebook": "Facebook",
        "instructor.name": "Instructor name",
        "instructor.twitter": "Twitter",
        "lookup.instructions": "For a summary prepared by your instructor, please enter your code below.",
        "lookup.invalid": "Please enter the code that your instructor gave you.",
        "lookup.submit": "Lookup",
        "page.allow_comments": "Allow comments",
        "page.allow_photo_downloads": "Allow photo downloads",
        "page.allow_photo_mapping": "Show photos on the map",
        "page.allow_sharing": "Allow sharing",
        "page.allow_video_downloads": "Allow video downloads",
        "page.body": "Body",
        "page.cancel": "Cancel",
        "page.code": "Client code",
        "page.comments": "Comments",
        "page.comments.add": "Leave a comment",
        "page.comments.body": "Comment",
        "page.comments.delete": "Delete",
        "page.comments.empty": "There are no comments yet.",
        "page.comments.invalid": "Please fill in all fields.",
        "page.comments.name": "Name",
        "page.comments.photo.approve": "Approve",
        "page.comments.photo.deny": "Deny",
        "page.comments.photo.remove": "Remove",
        "page.comments.post": "Post your comment",
        "page.comments.unapproved": "This comment has photos awaiting approval.",
        "page.create": "Create a page",
        "page.created": "Creation date",
        "page.date": "Date",
        "page.delete": "Delete this page",
        "page.delete.confirmation": "Are you sure you want to delete this page?",
        "page.edit": "Edit this page",
        "page.file.background": "Set as page background",
        "page.file.delete": "Remove",
        "page.file.description": "Description",
        "page.instructor": "Your instructor",
        "page.instructors": "Your instrutors",
        "page.logs": "GPS log",
        "page.logs.help": "GPS logs must be GPX, KML, ou TCX.",
        "page.map": "Map",
        "page.notes": "Private notes",
        "page.notes.help": "Your notes are not visible to your clients or your ski school.",
        "page.options": "Options",
        "page.other_instructors": "Other instructors",
        "page.other_instructors.help": "Specify other instructors that should be shown by entering their names.",
        "page.other_instructors.remove": "Remove",
        "page.photos": "Photos",
        "page.photos.download": "download",
        "page.photos.help": "Photos must be JPG, JPEG, ou PNG.",
        "page.save": "Save",
        "page.share": "Share the page",
        "page.school": "Your ski school",
        "page.stats": "Statistics",
        "page.stats.distance": "Total distance",
        "page.stats.final": "Final altitude",
        "page.stats.initial": "Initial altitude",
        "page.stats.maximum": "Maximum altitude",
        "page.stats.minimum": "Minimum altitude",
        "page.title": "Title",
        "page.untitled": "Untitled",
        "page.videos": "Videos",
        "page.videos.download": "download",
        "page.videos.help": "Videos must be AVI, MOV, MP4, MPG, WMV, or MTS.",
        "page.videos.notice": "Note: If you have just uploaded a video, it may take a few minutes to be processed and made available.",
        "school.booking": "Reservation site",
        "school.facebook": "Facebook",
        "school.mail": "Contact us",
        "school.twitter": "Twitter",
        "school.url": "Website",
        "site.name": "ESFBOOK",
        'terms': "Terms and conditions",
        tutorials: "Tutorials",
        "upload.cancel": "Cancel",
        "upload.complete": "Complete!",
        "upload.finalizing": "Finalizing...",
        "upload.starting": "Starting...",
        "upload.uploading": "Uploading...",
        "user.access": "Account access",
        "user.account": "My account",
        "user.bio": "Biography",
        "user.bio.help": "If you like, write a little about yourself.",
        "user.booking": "Reservation site",
        "user.cancel": "Cancel",
        "user.create": "Create an account",
        "user.created": "Registered",
        "user.dob": "Date of birth",
        "user.dob.help": "Your date of birth allows your identity to be verified in case of a problem with your account and is not visible on your profile.",
        "user.edit": "Edit my profile",
        "user.facebook": "Facebook account",
        "user.facebook.help": "Enter the Facebook URL that appears in your browser, e.g. https://www.facebook.com/ligety",
        "user.first": "First name",
        "user.forgot.identifier": "Card number or email address",
        "user.forgot.instructions": "Reset your password",
        "user.forgot.invalid": "Invalid username or password.",
        "user.forgot.recover": "Receive a new password by email",
        "user.full": "Full name",
        "user.inactive.copy": "Contact your school or click here for assistance.",
        "user.inactive.instructions": "Your membership has expired",
        "user.instructors": "My instructors",
        "user.instructors.filter.after": "After",
        "user.instructors.filter.before": "Before",
        "user.instructors.filter.created": "Filter by registration date",
        "user.instructors.filter.to": "to",
        "user.instructors.filter.search": "Filter by card number or name",
        "user.instructors.none": "No instructors have registered.",
        "user.last": "Last name",
        "user.login": "Log in",
        "user.login.forgot": "Forgot your password? Click here.",
        "user.login.instructions": "Please log in",
        "user.login.invalid": "Incorrect username or password.",
        "user.login.password": "Password",
        "user.login.submit": "Log in",
        "user.login.username": "Card number or school ID",
        "user.logout": "Log out",
        "user.mail": "Email address",
        "user.mail.help": "A valid email address. The system will send all emails to this address, including the email containing your password.",
        "user.name": "Name",
        "user.pages": "My pages",
        "user.pages.code": "Client code",
        "user.pages.date": "Creation date",
        "user.pages.filter.code": "Search by code",
        "user.pages.filter.date": "Filter by date",
        "user.pages.filter.end": "End date",
        "user.pages.filter.start": "Start date",
        "user.pages.filter.to": "to",
        "user.pages.none": "You have not created any pages yet.",
        "user.pages.title": "Page title",
        "user.pages.views": "Views",
        "user.password": "Password",
        "user.password.confirm": "Confirm your new password",
        "user.password.instructions": "Change my password",
        "user.password.password": "New password",
        "user.phone": "Phone number",
        "user.picture": "Profile photo",
        "user.picture.instructions": "Drop a new profile photo here",
        "user.picture.remove": "Remove",
        "user.profile": "My profile",
        "user.register.create": "Create an account",
        "user.register.help": "For more information about this service, click here.",
        "user.register.instructions": "Haven't registered yet?",
        "user.save": "Save",
        "user.school": "School",
        "user.school.help": "If your school is not in the list, click here for information on how to join.",
        "user.statistics": "Statistics",
        "user.statistics.instructors.count": "Instructors registered",
        "user.statistics.pages.created": "Pages created",
        "user.statistics.pages.views": "Number of views",
        "user.subpages": "My instructors' pages",
        "user.subpages.none": "Your instructors have not created any pages yet.",
        "user.twitter": "Twitter account",
        "user.twitter.help": "Enter your Twitter handle, ex. jeandupond",
        "user.url": "Website",
        "user.username": "Card number",
        "user.username.help": "You will log in by using your card number.",
        "user.verify.copy": "Your password has been sent to you by email.",
        "user.verify.instructions": "Thank you for registering"
    };
});
define("esfbook/locales/fr/config", ["exports"], function (exports) {
  // Ember-I18n includes configuration for common locales. Most users
  // can safely delete this file. Use it if you need to override behavior
  // for a locale or define behavior for a locale that Ember-I18n
  // doesn't know about.
  exports["default"] = {
    // rtl: [true|FALSE],
    //
    // pluralForm: function(count) {
    //   if (count === 0) { return 'zero'; }
    //   if (count === 1) { return 'one'; }
    //   if (count === 2) { return 'two'; }
    //   if (count < 5) { return 'few'; }
    //   if (count >= 5) { return 'many'; }
    //   return 'other';
    // }
  };
});
define("esfbook/locales/fr/translations", ["exports"], function (exports) {
    exports["default"] = {
        contact: "Assistance",
        credits: "Mentions légales",
        "datepicker.days.monday": "lundi",
        "datepicker.days.tuesday": "mardi",
        "datepicker.days.wednesday": "mercredi",
        "datepicker.days.thursday": "jeudi",
        "datepicker.days.friday": "vendredi",
        "datepicker.days.saturday": "samedi",
        "datepicker.days.sunday": "dimanche",
        "datepicker.days.short.monday": "lun",
        "datepicker.days.short.tuesday": "mar",
        "datepicker.days.short.wednesday": "mer",
        "datepicker.days.short.thursday": "jeu",
        "datepicker.days.short.friday": "ven",
        "datepicker.days.short.saturday": "sam",
        "datepicker.days.short.sunday": "dim",
        "datepicker.months.january": "janvier",
        "datepicker.months.february": "février",
        "datepicker.months.march": "mars",
        "datepicker.months.april": "avril",
        "datepicker.months.may": "mai",
        "datepicker.months.june": "juin",
        "datepicker.months.july": "juillet",
        "datepicker.months.august": "août",
        "datepicker.months.september": "septembre",
        "datepicker.months.october": "octobre",
        "datepicker.months.november": "novembre",
        "datepicker.months.december": "décembre",
        "datepicker.next": ">",
        "datepicker.previous": "<",
        "dropzone.multiple": "Cliquez ou déposez vos fichiers à télécharger ici",
        "dropzone.single": "Cliquez ou déposez votre fichier à télécharger ici",
        "editor.bold": "Gras",
        "editor.italic": "Italique",
        "editor.link": "Lien",
        "editor.link.cancel": "Annuler",
        "editor.link.ok": "OK",
        "editor.ordered": "Liste numérotée",
        "editor.underline": "Souligné",
        "editor.unordered": "Liste à puces",
        "error.content.not_found": "The requested content could not be found.",
        "error.page.not_found.message": "La page est introuvable",
        "error.page.not_found.return": "Retour à la page d'accueil",
        "error.server": "An error occured. Please try again in a few minutes.",
        "error.token.bad_credentials": "Incorrect username or password.",
        "error.token.general": "Login failed.",
        "error.token.missing_username": "Please enter a valid username.",
        "error.vimeo.general": "Your video could not be uploaded because of an issue with our provider. Please try again later.",
        help: "Aide en ligne",
        "instructor.bio": "Biographie",
        "instructor.email": "Me contacter",
        "instructor.facebook": "Facebook",
        "instructor.name": "Nom du moniteur",
        "instructor.twitter": "Twitter",
        "lookup.instructions": "Pour consulter le résumé que votre moniteur vous a préparé, entrez votre code ci-dessous.",
        "lookup.invalid": "Veuillez entrer le code que votre moniteur vous a donné.",
        "lookup.submit": "Valider",
        "page.allow_comments": "Commentaires autorisés",
        "page.allow_photo_downloads": "Permettre le téléchargement des photos",
        "page.allow_photo_mapping": "Afficher sur la carte les photos géolocalisées",
        "page.allow_sharing": "Permettre le partage",
        "page.allow_video_downloads": "Permettre le téléchargement des vidéos",
        "page.body": "Résumé",
        "page.cancel": "Annuler",
        "page.code": "Code client",
        "page.comments": "Commentaires",
        "page.comments.add": "Laisser un commentaire",
        "page.comments.body": "Commentaire",
        "page.comments.delete": "Supprimer",
        "page.comments.empty": "Il n'y a pas encore de commentaires.",
        "page.comments.invalid": "Veuillez remplir tous les champs.",
        "page.comments.name": "Nom",
        "page.comments.photo.approve": "Approuver",
        "page.comments.photo.deny": "Nier",
        "page.comments.photo.remove": "Supprimer",
        "page.comments.post": "Poster votre commentaire",
        "page.comments.unapproved": "Ce commentaire a des photos en attente d'approbation.",
        "page.create": "Créer une page",
        "page.created": "Date de publication",
        "page.date": "Date",
        "page.delete": "Supprimer cette page",
        "page.delete.confirmation": "Etes-vous sûr de vouloir supprimer cette page?",
        "page.edit": "Modifier cette page",
        "page.file.background": "Définir comme image de fond",
        "page.file.delete": "Supprimer",
        "page.file.description": "Description",
        "page.instructor": "Votre moniteur",
        "page.instructors": "Vos moniteurs",
        "page.logs": "Fichier GPS",
        "page.logs.help": "Votre fichier GPS doit être au format GPX, KML, ou TCX.",
        "page.map": "Itinéraire",
        "page.notes": "Notes privées",
        "page.notes.help": "Vos notes ne sont pas visibles par vos clients ni par votre ESF.",
        "page.options": "Options",
        "page.other_instructors": "Autres moniteurs",
        "page.other_instructors.help": "Indiquez si d'autres moniteurs doivent être mentionnés sur cette page en écrivant leurs noms.",
        "page.other_instructors.remove": "Supprimer",
        "page.photos": "Photos",
        "page.photos.download": "télécharger",
        "page.photos.help": "Vos fichiers photos doivent être au format JPG, JPEG, ou PNG.",
        "page.save": "Enregistrer",
        "page.share": "Partager la page",
        "page.school": "Votre école de ski",
        "page.stats": "Statistiques",
        "page.stats.distance": "Distance totale",
        "page.stats.final": "Altitude à l'arrivée",
        "page.stats.initial": "Altitude au départ",
        "page.stats.maximum": "Altitude maxi",
        "page.stats.minimum": "Altitude mini",
        "page.title": "Titre",
        "page.untitled": "Page sans titre",
        "page.videos": "Vidéos",
        "page.videos.download": "télécharger",
        "page.videos.help": "Vos fichiers vidéos doivent être au format AVI, MOV, MP4, MPG, WMV, ou MTS.",
        "page.videos.notice": "Note: Si vous venez de mettre une vidéo en ligne, comptez quelques minutes avant qu'elle ne soit visible.",
        "school.booking": "Réserver en ligne",
        "school.facebook": "Facebook",
        "school.mail": "Nous contacter",
        "school.twitter": "Twitter",
        "school.url": "Site internet",
        "site.name": "ESFBOOK",
        'terms': "Conditions générales",
        'tutorials': "Tutoriels",
        "upload.cancel": "Annuler",
        "upload.complete": "Complete!",
        "upload.finalizing": "Finalizing...",
        "upload.starting": "Starting...",
        "upload.uploading": "Uploading...",
        "user.access": "Accès moniteur",
        "user.account": "Mon compte",
        "user.bio": "Biographie",
        "user.bio.help": "Vous pouvez si vous le souhaitez ajouter une courte introduction pour vous présenter.",
        "user.booking": "Site de réservations",
        "user.cancel": "Annuler",
        "user.create": "Compte utilisateur",
        "user.created": "Inscription",
        "user.dob": "Date de naissance",
        "user.dob.help": "Votre date de naissance servira à vérifier votre identité en cas de problème mais ne sera pas visible sur votre profil.",
        "user.edit": "Modifier mon profil",
        "user.facebook": "Compte Facebook",
        "user.facebook.help": "Regardez sur votre page Facebook l'URL qui s'affiche dans votre navigateur, ex. https://www.facebook.com/ligety",
        "user.first": "Prenom",
        "user.forgot.identifier": "N° de carte syndicale ou e-mail",
        "user.forgot.instructions": "Réinitialiser votre mot de passe",
        "user.forgot.invalid": "Identifiant invalide.",
        "user.forgot.recover": "Recevoir un nouveau mot de passe par e-mail",
        "user.full": "Nom et prénom",
        "user.inactive.copy": "Contactez votre école ou cliquez ici pour de l'aide.",
        "user.inactive.instructions": "Votre adhésion a expiré",
        "user.instructors": "Mes moniteurs",
        "user.instructors.filter.after": "After",
        "user.instructors.filter.before": "Before",
        "user.instructors.filter.created": "Filtrer par date d'inscription",
        "user.instructors.filter.to": "au",
        "user.instructors.filter.search": "Filtrer par N° de carte ou nom",
        "user.instructors.none": "Aucun instructeurs ont encore enregistré.",
        "user.last": "Nom",
        "user.login": "Se connecter",
        "user.login.forgot": "Mot de passe oublié ? Cliquez ici.",
        "user.login.instructions": "Saisissez votre identifiant",
        "user.login.invalid": "Identifiant ou mot de passe invalide.",
        "user.login.password": "Mot de passe",
        "user.login.submit": "Se connecter",
        "user.login.username": "N° de carte syndicale ou N° ESF",
        "user.logout": "Se déconnecter",
        "user.mail": "Adresse de courriel",
        "user.mail.help": "Une adresse électronique valide. Le système enverra tous les courriels à cette adresse, dont le mail contenant votre mot de passe.",
        "user.name": "Nom de l'ESF",
        "user.pages": "Mes pages",
        "user.pages.code": "Code client",
        "user.pages.date": "Date de publication",
        "user.pages.filter.code": "Rechercher par code",
        "user.pages.filter.date": "Filtrer par date",
        "user.pages.filter.end": "End date",
        "user.pages.filter.start": "Start date",
        "user.pages.filter.to": "au",
        "user.pages.none": "Vous n'avez pas encore créé de pages.",
        "user.pages.title": "Titre de la page",
        "user.pages.views": "Vues",
        "user.password": "Mot de passe",
        "user.password.confirm": "Confirmer le nouveau mot de passe",
        "user.password.instructions": "Changer de mot de passe",
        "user.password.password": "Nouveau mot de passe",
        "user.phone": "Numéro de téléphone",
        "user.picture": "Photo du profil",
        "user.picture.instructions": "Déposer une nouvelle photo de profil ici",
        "user.picture.remove": "Supprimer",
        "user.profile": "Mon profil",
        "user.register.create": "Créer un compte",
        "user.register.help": "Pour plus d'information sur le service, cliquez ici.",
        "user.register.instructions": "Pas encore inscrit ?",
        "user.save": "Enregistrer",
        "user.school": "Ecole de ski",
        "user.school.help": "Si votre ESF n'est pas présente dans la liste, cliquez ici pour plus d'informations sur comment adhérer.",
        "user.statistics": "Statistiques",
        "user.statistics.instructors.count": "Moniteurs inscrits",
        "user.statistics.pages.created": "Pages créées",
        "user.statistics.pages.views": "Nombre de vues",
        "user.subpages": "Les pages de mes moniteurs",
        "user.subpages.none": "Vos instructeurs ont pas encore créé de pages.",
        "user.twitter": "Compte Twitter",
        "user.twitter.help": "Indiquez votre identifiant sur twitter, ex. jeandupond",
        "user.url": "Site internet",
        "user.username": "N° de carte",
        "user.username.help": "Votre numéro de carte syndicale sera votre identifiant pour vous connecter.",
        "user.verify.copy": "Votre mot de passe a été envoyé à votre adresse email.",
        "user.verify.instructions": "Merci pour votre inscription"
    };
});
define("esfbook/mirage/config", ["exports"], function (exports) {
  exports["default"] = function () {}

  // These comments are here to help you get started. Feel free to delete them.

  /*
    Config (with defaults).
     Note: these only affect routes defined *after* them!
  */
  // this.urlPrefix = '';    // make this `http://localhost:8080`, for example, if your API is on a different server
  // this.namespace = '';    // make this `api`, for example, if your API is namespaced
  // this.timing = 400;      // delay for each request, automatically set to 0 during testing

  /*
    Route shorthand cheatsheet
  */
  /*
    GET shorthands
     // Collections
    this.get('/contacts');
    this.get('/contacts', 'users');
    this.get('/contacts', ['contacts', 'addresses']);
     // Single objects
    this.get('/contacts/:id');
    this.get('/contacts/:id', 'user');
    this.get('/contacts/:id', ['contact', 'addresses']);
  */

  /*
    POST shorthands
     this.post('/contacts');
    this.post('/contacts', 'user'); // specify the type of resource to be created
  */

  /*
    PUT shorthands
     this.put('/contacts/:id');
    this.put('/contacts/:id', 'user'); // specify the type of resource to be updated
  */

  /*
    DELETE shorthands
     this.del('/contacts/:id');
    this.del('/contacts/:id', 'user'); // specify the type of resource to be deleted
     // Single object + related resources. Make sure parent resource is first.
    this.del('/contacts/:id', ['contact', 'addresses']);
  */

  /*
    Function fallback. Manipulate data in the db via
       - db.{collection}
      - db.{collection}.find(id)
      - db.{collection}.where(query)
      - db.{collection}.update(target, attrs)
      - db.{collection}.remove(target)
     // Example: return a single object with related models
    this.get('/contacts/:id', function(db, request) {
      var contactId = +request.params.id;
       return {
        contact: db.contacts.find(contactId),
        addresses: db.addresses.where({contact_id: contactId})
      };
    });
   */

  /*
  You can optionally export a config that is only loaded during tests
  export function testConfig() {
  
  }
  */
  ;
});
define('esfbook/mirage/factories/contact', ['exports', 'ember-cli-mirage'], function (exports, _emberCliMirage) {
  exports['default'] = _emberCliMirage['default'].Factory.extend({
    // name: 'Pete',                         // strings
    // age: 20,                              // numbers
    // tall: true,                           // booleans

    // email: function(i) {                  // and functions
    //   return 'person' + i + '@test.com';
    // },

    // firstName: faker.name.firstName,       // using faker
    // lastName: faker.name.firstName,
    // zipCode: faker.address.zipCode
  });
});
/*
  This is an example factory definition.

  Create more files in this directory to define additional factories.
*/
/*, {faker} */
define("esfbook/mirage/scenarios/default", ["exports"], function (exports) {
  exports["default"] = function () /* server */{

    // Seed your development database using your factories. This
    // data will not be loaded in your tests.

    // server.createList('contact', 10);
  };
});
define('esfbook/models/user', ['exports', 'ember-data'], function (exports, _emberData) {
  exports['default'] = _emberData['default'].Model.extend({
    username: _emberData['default'].attr('string')
  });
});
// app/user/model.js
define('esfbook/resolver', ['exports', 'ember-resolver'], function (exports, _emberResolver) {
  exports['default'] = _emberResolver['default'];
});
define('esfbook/router', ['exports', 'ember', 'esfbook/config/environment'], function (exports, _ember, _esfbookConfigEnvironment) {

  var Router = _ember['default'].Router.extend({
    location: _esfbookConfigEnvironment['default'].locationType
  });

  Router.map(function () {
    this.route('home', { path: '/' });
    this.route('user', { path: '/user/:id' }, function () {
      this.route('login');
    });
    this.route('page', { path: '/page' });
    this.route('404');
  });

  exports['default'] = Router;
});
define('esfbook/routes/404', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Route.extend({});
});
define('esfbook/routes/application', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Route.extend({});
});
define('esfbook/routes/home', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Route.extend({});
});
define('esfbook/routes/page', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Route.extend({});
});
define('esfbook/routes/user/login', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Route.extend({});
});
define('esfbook/routes/user', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Route.extend({});
});
define('esfbook/services/ajax', ['exports', 'ember-ajax/services/ajax'], function (exports, _emberAjaxServicesAjax) {
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberAjaxServicesAjax['default'];
    }
  });
});
define('esfbook/services/i18n', ['exports', 'ember-i18n/services/i18n'], function (exports, _emberI18nServicesI18n) {
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberI18nServicesI18n['default'];
    }
  });
});
define("esfbook/templates/404", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "fragmentReason": {
          "name": "missing-wrapper",
          "problems": ["wrong-type"]
        },
        "revision": "Ember@2.5.1",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 2,
            "column": 0
          }
        },
        "moduleName": "esfbook/templates/404.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createTextNode("404: Page Not Found\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes() {
        return [];
      },
      statements: [],
      locals: [],
      templates: []
    };
  })());
});
define("esfbook/templates/application", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "fragmentReason": {
          "name": "missing-wrapper",
          "problems": ["wrong-type", "multiple-nodes"]
        },
        "revision": "Ember@2.5.1",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 42,
            "column": 0
          }
        },
        "moduleName": "esfbook/templates/application.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment(" app/application/template.hbs ");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createComment(" Footer, for every page, does not change language ");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "wrapper");
        var el2 = dom.createTextNode("\n  ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "application");
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("header");
        var el4 = dom.createTextNode("\n      ");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("div");
        dom.setAttribute(el4, "class", "brand");
        var el5 = dom.createTextNode("\n        ");
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("div");
        dom.setAttribute(el5, "class", "row");
        var el6 = dom.createTextNode("\n          ");
        dom.appendChild(el5, el6);
        var el6 = dom.createElement("div");
        dom.setAttribute(el6, "class", "medium-6 columns");
        var el7 = dom.createTextNode("\n            ");
        dom.appendChild(el6, el7);
        var el7 = dom.createElement("a");
        dom.setAttribute(el7, "class", "ember-logo brand-logo active");
        dom.setAttribute(el7, "href", "/");
        var el8 = dom.createTextNode("\n              ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("img");
        dom.setAttribute(el8, "class", "brand-logo");
        dom.setAttribute(el8, "src", "/images/logo.png");
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n            ");
        dom.appendChild(el7, el8);
        dom.appendChild(el6, el7);
        var el7 = dom.createTextNode("\n          ");
        dom.appendChild(el6, el7);
        dom.appendChild(el5, el6);
        var el6 = dom.createTextNode("\n          ");
        dom.appendChild(el5, el6);
        var el6 = dom.createElement("div");
        dom.setAttribute(el6, "class", "medium-6 columns");
        var el7 = dom.createTextNode("\n            ");
        dom.appendChild(el6, el7);
        var el7 = dom.createElement("a");
        var el8 = dom.createTextNode("FR");
        dom.appendChild(el7, el8);
        dom.appendChild(el6, el7);
        var el7 = dom.createTextNode("\n            ");
        dom.appendChild(el6, el7);
        var el7 = dom.createElement("a");
        var el8 = dom.createTextNode("EN");
        dom.appendChild(el7, el8);
        dom.appendChild(el6, el7);
        var el7 = dom.createTextNode("\n          ");
        dom.appendChild(el6, el7);
        dom.appendChild(el5, el6);
        var el6 = dom.createTextNode("\n        ");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("\n      ");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n    ");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n\n    ");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n\n\n\n  ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n  \n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createComment(" Footer, for every page, changes depending on status of fr ");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "footer-menu");
        var el2 = dom.createTextNode("\n  ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("a");
        dom.setAttribute(el2, "class", "menu-item");
        dom.setAttribute(el2, "href", "/user/login");
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("span");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n  ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n  ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("a");
        dom.setAttribute(el2, "class", "menu-item");
        dom.setAttribute(el2, "href", "http://info.esfbook.net/conditions-generales/");
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("span");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n  ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n  ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("a");
        dom.setAttribute(el2, "class", "menu-item");
        dom.setAttribute(el2, "href", "http://info.esfbook.net/mentions-legales");
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("span");
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n  ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element0 = dom.childAt(fragment, [4, 1]);
        var element1 = dom.childAt(element0, [1, 1, 1, 3]);
        var element2 = dom.childAt(element1, [1]);
        var element3 = dom.childAt(element1, [3]);
        var element4 = dom.childAt(fragment, [8]);
        var morphs = new Array(8);
        morphs[0] = dom.createAttrMorph(element2, 'class');
        morphs[1] = dom.createElementMorph(element2);
        morphs[2] = dom.createAttrMorph(element3, 'class');
        morphs[3] = dom.createElementMorph(element3);
        morphs[4] = dom.createMorphAt(element0, 3, 3);
        morphs[5] = dom.createMorphAt(dom.childAt(element4, [1, 1]), 0, 0);
        morphs[6] = dom.createMorphAt(dom.childAt(element4, [3, 1]), 0, 0);
        morphs[7] = dom.createMorphAt(dom.childAt(element4, [5, 1]), 0, 0);
        return morphs;
      },
      statements: [["attribute", "class", ["concat", ["fr-toggle ", ["get", "locales.isFr", ["loc", [null, [15, 34], [15, 46]]]]]]], ["element", "action", ["makeFrActive"], [], ["loc", [null, [15, 50], [15, 75]]]], ["attribute", "class", ["concat", ["en-toggle ", ["get", "locales.isEn", ["loc", [null, [16, 34], [16, 46]]]]]]], ["element", "action", ["makeEnActive"], [], ["loc", [null, [16, 50], [16, 75]]]], ["content", "outlet", ["loc", [null, [22, 4], [22, 14]]]], ["content", "locales.accountAccess", ["loc", [null, [33, 10], [33, 35]]]], ["content", "locales.terms", ["loc", [null, [36, 10], [36, 27]]]], ["content", "locales.credits", ["loc", [null, [39, 10], [39, 29]]]]],
      locals: [],
      templates: []
    };
  })());
});
define("esfbook/templates/components/comment-frame", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "fragmentReason": {
          "name": "missing-wrapper",
          "problems": ["wrong-type"]
        },
        "revision": "Ember@2.5.1",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 2,
            "column": 0
          }
        },
        "moduleName": "esfbook/templates/components/comment-frame.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        return morphs;
      },
      statements: [["content", "yield", ["loc", [null, [1, 0], [1, 9]]]]],
      locals: [],
      templates: []
    };
  })());
});
define("esfbook/templates/components/gps-frame", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "fragmentReason": {
          "name": "missing-wrapper",
          "problems": ["wrong-type"]
        },
        "revision": "Ember@2.5.1",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 2,
            "column": 0
          }
        },
        "moduleName": "esfbook/templates/components/gps-frame.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        return morphs;
      },
      statements: [["content", "yield", ["loc", [null, [1, 0], [1, 9]]]]],
      locals: [],
      templates: []
    };
  })());
});
define("esfbook/templates/components/message-frame", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "fragmentReason": {
          "name": "missing-wrapper",
          "problems": ["wrong-type"]
        },
        "revision": "Ember@2.5.1",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 2,
            "column": 0
          }
        },
        "moduleName": "esfbook/templates/components/message-frame.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        return morphs;
      },
      statements: [["content", "yield", ["loc", [null, [1, 0], [1, 9]]]]],
      locals: [],
      templates: []
    };
  })());
});
define("esfbook/templates/components/photo-frame", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "fragmentReason": {
          "name": "missing-wrapper",
          "problems": ["wrong-type"]
        },
        "revision": "Ember@2.5.1",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 2,
            "column": 0
          }
        },
        "moduleName": "esfbook/templates/components/photo-frame.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        return morphs;
      },
      statements: [["content", "yield", ["loc", [null, [1, 0], [1, 9]]]]],
      locals: [],
      templates: []
    };
  })());
});
define("esfbook/templates/components/picture-frame", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "fragmentReason": {
          "name": "missing-wrapper",
          "problems": ["wrong-type"]
        },
        "revision": "Ember@2.5.1",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 2,
            "column": 0
          }
        },
        "moduleName": "esfbook/templates/components/picture-frame.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        return morphs;
      },
      statements: [["content", "yield", ["loc", [null, [1, 0], [1, 9]]]]],
      locals: [],
      templates: []
    };
  })());
});
define("esfbook/templates/components/video-frame", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "fragmentReason": {
          "name": "missing-wrapper",
          "problems": ["wrong-type"]
        },
        "revision": "Ember@2.5.1",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 2,
            "column": 0
          }
        },
        "moduleName": "esfbook/templates/components/video-frame.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        return morphs;
      },
      statements: [["content", "yield", ["loc", [null, [1, 0], [1, 9]]]]],
      locals: [],
      templates: []
    };
  })());
});
define("esfbook/templates/home", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "fragmentReason": {
          "name": "missing-wrapper",
          "problems": ["multiple-nodes", "wrong-type"]
        },
        "revision": "Ember@2.5.1",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 18,
            "column": 0
          }
        },
        "moduleName": "esfbook/templates/home.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "row");
        var el2 = dom.createTextNode("\n  ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "small-12 columns");
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "lookup-box");
        var el4 = dom.createTextNode("\n      ");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("div");
        dom.setAttribute(el4, "class", "instructions");
        var el5 = dom.createTextNode("\n        ");
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("span");
        var el6 = dom.createComment("");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("\n      ");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n      ");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("form");
        dom.setAttribute(el4, "class", "form");
        var el5 = dom.createTextNode("\n        ");
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("input");
        dom.setAttribute(el5, "class", "ember-view ember-text-field form-text");
        dom.setAttribute(el5, "type", "text");
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("\n        ");
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("button");
        dom.setAttribute(el5, "class", "form-button blue");
        dom.setAttribute(el5, "type", "submit");
        var el6 = dom.createTextNode("\n          ");
        dom.appendChild(el5, el6);
        var el6 = dom.createElement("span");
        var el7 = dom.createTextNode(" ");
        dom.appendChild(el6, el7);
        var el7 = dom.createComment("");
        dom.appendChild(el6, el7);
        dom.appendChild(el5, el6);
        var el6 = dom.createTextNode("\n        ");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("\n      ");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n    ");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n  ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element0 = dom.childAt(fragment, [0, 1, 1]);
        var morphs = new Array(3);
        morphs[0] = dom.createMorphAt(dom.childAt(element0, [1, 1]), 0, 0);
        morphs[1] = dom.createMorphAt(dom.childAt(element0, [3, 3, 1]), 1, 1);
        morphs[2] = dom.createMorphAt(fragment, 2, 2, contextualElement);
        return morphs;
      },
      statements: [["content", "locales.codeInstructions", ["loc", [null, [5, 14], [5, 42]]]], ["content", "locales.submit", ["loc", [null, [10, 17], [10, 35]]]], ["content", "outlet", ["loc", [null, [17, 0], [17, 10]]]]],
      locals: [],
      templates: []
    };
  })());
});
define("esfbook/templates/page", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "fragmentReason": {
          "name": "missing-wrapper",
          "problems": ["wrong-type"]
        },
        "revision": "Ember@2.5.1",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 2,
            "column": 0
          }
        },
        "moduleName": "esfbook/templates/page.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        return morphs;
      },
      statements: [["content", "outlet", ["loc", [null, [1, 0], [1, 10]]]]],
      locals: [],
      templates: []
    };
  })());
});
define("esfbook/templates/user/login", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "fragmentReason": {
          "name": "missing-wrapper",
          "problems": ["wrong-type"]
        },
        "revision": "Ember@2.5.1",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 2,
            "column": 0
          }
        },
        "moduleName": "esfbook/templates/user/login.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        return morphs;
      },
      statements: [["content", "outlet", ["loc", [null, [1, 0], [1, 10]]]]],
      locals: [],
      templates: []
    };
  })());
});
define("esfbook/templates/user", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "fragmentReason": {
          "name": "missing-wrapper",
          "problems": ["wrong-type"]
        },
        "revision": "Ember@2.5.1",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 2,
            "column": 0
          }
        },
        "moduleName": "esfbook/templates/user.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        return morphs;
      },
      statements: [["content", "outlet", ["loc", [null, [1, 0], [1, 10]]]]],
      locals: [],
      templates: []
    };
  })());
});
define('esfbook/utils/i18n/compile-template', ['exports', 'ember-i18n/utils/i18n/compile-template'], function (exports, _emberI18nUtilsI18nCompileTemplate) {
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberI18nUtilsI18nCompileTemplate['default'];
    }
  });
});
define('esfbook/utils/i18n/missing-message', ['exports', 'ember-i18n/utils/i18n/missing-message'], function (exports, _emberI18nUtilsI18nMissingMessage) {
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberI18nUtilsI18nMissingMessage['default'];
    }
  });
});
/* jshint ignore:start */



/* jshint ignore:end */

/* jshint ignore:start */

define('esfbook/config/environment', ['ember'], function(Ember) {
  var prefix = 'esfbook';
/* jshint ignore:start */

try {
  var metaName = prefix + '/config/environment';
  var rawConfig = Ember['default'].$('meta[name="' + metaName + '"]').attr('content');
  var config = JSON.parse(unescape(rawConfig));

  return { 'default': config };
}
catch(err) {
  throw new Error('Could not read config from meta tag with name "' + metaName + '".');
}

/* jshint ignore:end */

});

/* jshint ignore:end */

/* jshint ignore:start */

if (!runningTests) {
  require("esfbook/app")["default"].create({"name":"esfbook","version":"0.0.0+5c42a206"});
}

/* jshint ignore:end */
//# sourceMappingURL=esfbook.map